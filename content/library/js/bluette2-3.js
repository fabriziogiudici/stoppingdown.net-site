/*
 *
 * Copyright (c) 2013-2015 by Fabrizio Giudici
 *
 */

$(document).ready(function()
  {
    // TODO: fade-in del titolo
    // TODO: when clicking on a splash link, fade out and show initialLoadingWidget before switching URL
    // TODO: in case of error, hide title and overload
    // 
    // TODO: small devices, move the buttons to the left, vertically

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
    var updateTitleAndUrl = function(fancybox, current, previous)
      {
        var index = fancybox.index;
        var group = fancybox.group;
        var title = fancybox.title;
        var id = $(group[index]).attr('id');
        debug("Current id: %s", id);

        fancybox.title = (index + 1) + ' / ' + group.length + (title ? ' - ' + title : '');

        if (bluetteUpdateUrl)
          {
            location.href = baseUrl + "#!/" + id;
          }

        if (bluetteUpdateTitle)
          {
            document.title = bluetteTitlePrefix + title;
          }
      };

/***********************************************************************************************************************
 *
 * Retrieves the catalog of photos.
 *
 **********************************************************************************************************************/
    var loadCatalog = function()
      {
        info("loadCatalog()");
        debug("Loading %s", bluetteCatalogUrl);

        $.ajax(
          {
            type:     "GET",
            url:      bluetteCatalogUrl,
            datatype: "xml",
            success:  parseCatalog,
            error:    function (xmlHttpRequest, textStatus, errorThrown)
              {
                fatal("Cannot load the catalog: " + xmlHttpRequest.responseText);
              }
          });
      };

/***********************************************************************************************************************
 *
 * Opens the LightBox.
 *
 **********************************************************************************************************************/
    var openLightBox = function()
      {
        info("openLightBox()");
        $("#slideshow").fadeOut(function()
          {
            $.fancybox.close(true);
            slideShowVisible = false;
            $("#initialLoadingWidget").hide();
            $("#loadingWidget").hide();
            $("#waitingWidget").hide();
            location.href = baseUrl + "#!/lightbox";

            $("#lightbox").fadeIn(function()
              {
                if (!thumbnailsLoaded)
                  {
                    loadThumbnails();
//                    setTimeout(loadThumbnails, 500);
                  }
              });
          });
      };

/***********************************************************************************************************************
 *
 * Closes the LightBox.
 *
 **********************************************************************************************************************/
    var closeLightBox = function()
      {
        info("closeLightBox()");
        $("#lightbox").fadeOut(function()
          {
            $("#slideshow").fadeIn(function()
              {
                startSlideshow();
              });
          });
      };

/***********************************************************************************************************************
 *
 * Loads the thumbnail in the LightBox. Images are loaded in background and rendered as they are ready.
 *
 **********************************************************************************************************************/
    var loadThumbnails = function()
      {
        info("loadThumbnails()");
        thumbnailsLoaded = true;
        var index = 0;

        $(photos).each(function()
          {
            var thisIndex = index++;
            var url = bluettePhotoUrl(this.id, computeMediaSize(neededMediaSize / bluetteThumbnailsPerRow));
            debug("Loading %s...", url);

            var img = $('<img/>').attr('src', url)
                                 .css({'display' : 'none'})
                                 .appendTo(scrollPaneApi.getContentPane())
                                 .click(function()
                                    {
                                      goToPhoto(thisIndex);
                                    });

            initializeThumbnail(img);
            img.load(function()
              {
                debug("Loaded %s...", this);
    //                var size = computeLargestFittingSize(this, // works everywhere but IE
                var size = computeLargestFittingSize(
                  {
                    width  : $(this).width(),
                    height : $(this).height()
                  },
                  {
                    width  : neededMediaSize / bluetteThumbnailsPerRow,
                    height : neededMediaSize / bluetteThumbnailsPerRow
                  });

                $(this).attr('width', size.width).attr('height', size.height).fadeIn();
                scrollPaneApi.reinitialise();
              });
          });
      };

/***********************************************************************************************************************
 *
 * Performs specific thumbnail initialization.
 *
 **********************************************************************************************************************/
    function initializeThumbnail (img)
      {
        var range = 5;
        var angle = Math.random() * range * 2 - range + 'deg';
        img.css({
                  '-webkit-transform' : 'rotate(' + angle + ')',
                  '-moz-transform'    : 'rotate(' + angle + ')'
                });
      }

/***********************************************************************************************************************
 *
 * Goes to the bluetteHome page.
 *
 **********************************************************************************************************************/
    var goHome = function()
      {
        info("goHome()");
        $("#slideshow").fadeOut(function()
          {
            setTimeout(function()
              {
                $.fancybox.close(true);
                location.href = bluetteHome;
              }, 500);
          });
      };

/***********************************************************************************************************************
 *
 * Goes to the specified photo.
 *
 **********************************************************************************************************************/
    var goToPhoto = function (index)
      {
        info("goToPhoto(%d)", index);
        autoPlay = false;
        firstIndex = index;
        closeLightBox();
      };

/***********************************************************************************************************************
 *
 * Loads the catalog of photos.
 *
 **********************************************************************************************************************/
    var parseCatalog = function (xml)
      {
        info("parseCatalog() - ", xml);

        $(xml).find("stillImage").each(function(i, img)
          {
            var $img  = $(img);
            var id    = $img.attr("id");
            var title = $img.attr("title");
            var photo = { "id": id, "title": title, "href": bluettePhotoUrl(id, neededMediaSize) };
            photos.push(photo);
          });

        debug("loaded %s items", photos.length);

        if (bluetteShuffle)
          {
            $($.shuffle(photos));
          }

//        $("#initialLoadingWidget").hide(); Will be hidden by afterLoad

        if (photos.length === 0)
          {
            fatal("Error: no photos in this slideshow");
          }
        else if (initialStatus === "lightbox")
          {
            openLightBox();
          }
        else
          {
            $(photos).each(function(index, photo)
              {
                if (photo.id === initialStatus)
                  {
                    firstIndex = index;
                    return false;
                  }
              });

            debug("Initial photo index: %d", firstIndex);
            startSlideshow();
          }
      };

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
    var startSlideshow = function()
      {
        info("startSlideshow()");
        buttonsBound = false;
        var customButtons = '<div id="fancybox-buttons"><ul><li><a class="btnPrev" title="Previous" href="javascript:;"></a></li><li><a class="btnLightbox" title="Lightbox" href="javascript:;"></a></li><li><a class="btnClose" title="Close" href="javascript:;"></a></li><li><a class="btnPlay" title="Start slideshow" href="javascript:;"></a></li><li><a class="btnNext" title="Next" href="javascript:;"></a></li></ul></div>';

        $.fancybox(photos,
          {
            parent:      'div[id="slideshow"]',

            nextSpeed:   1000,
            prevSpeed:   1000,
            nextEffect:  'fade',
            prevEffect:  'fade',

            nextClick:   bluetteNavigation,
            closeBtn:    false,
            arrows:      bluetteNavigation,

            closeEffect: 'fade',
            closeSpeed:  1000,

            index:       firstIndex,
            autoPlay:    autoPlay,
            playSpeed:   bluetteSlideshowSpeed,

            tpl:
              {
                loading: ''
              },

            beforeShow:  function()
              {
                $("#imageOverlay").clone(true, true).appendTo(".fancybox-inner").show();

                if ($.fancybox.player.isActive && bluetteShowProgressIcons)
                  {
                    $("#waitingWidget").hide();
                  }

                /* Disable right click */
                $.fancybox.wrap.bind("contextmenu", function (e)
                  {
                    return false;
                  });
              },

            afterShow:   function()
              {
                tryToBindButtons();

                if ($.fancybox.player.isActive && bluetteShowProgressIcons)
                  {
                    $("#waitingWidget").show();
                  }
              },

            beforeLoad:  function()
              {
                if (bluetteShowProgressIcons)
                  {
                    $("#waitingWidget").hide();

                    if ($(".fancybox-image") === null)
                      {
                        $("#initialLoadingWidget").show();
                      }
                    else
                      {
                        // TODO: delay 100msec before showing, canceled by afterLoad
                        $("#loadingWidget").show();
                      }
                  }
              },

            afterLoad:   function(current, previous)
              {
                $("#initialLoadingWidget").hide(); // always, so it closes the initial one

                if (bluetteShowProgressIcons)
                  {
                    $("#loadingWidget").hide();

                    if ($.fancybox.player.isActive)
                      {
                        $("#waitingWidget").show();
                      }
                  }

                updateTitleAndUrl(this, current, previous);
              },

            helpers:
              {
                overlay: { closeClick: false, fixed: false },

                title:   bluetteTitleVisible ? { type: 'inside' } : null,

                buttons: bluetteNavigation ? { tpl: customButtons } : null
              }
          });
      };

/***********************************************************************************************************************
 *
 * Buttons are created later. FIXME: better way to bind callbacks after initialization?
 *
 *
 **********************************************************************************************************************/
    function tryToBindButtons()
      {
        if (buttonsBound || !bluetteNavigation)
          {
            return;
          }

        var l = $.fancybox.helpers.buttons.list;

        if ((l === null) || (l.find(".btnLightbox") === null))
          {
            debug("buttons not ready, postponing binding...");
            setTimeout(tryToBindButtons, 100);
          }
        else
          {
            buttonsBound = true;
            bindButtons();
          }
      }

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
    var bindButtons = function()
      {
        debug("bindButtons()");
        var l = $.fancybox.helpers.buttons.list;
        l.find(".btnLightbox").click(openLightBox);
        l.find(".btnClose").click(goHome);
        l.find(".btnPlay").click(function()
          {
            if ($.fancybox.player.isActive)
              {
                $.fancybox.next();
                $.fancybox.play(true);
              }
            else
              {
                $("#waitingWidget").hide();
              }
          });
        l.find(".btnPrev").click(function()
          {
            $("#waitingWidget").hide();
            $.fancybox.play(false);
            $.fancybox.prev();
          });
        l.find(".btnNext").click(function()
          {
            $("#waitingWidget").hide();
            $.fancybox.play(false);
            $.fancybox.next();
          });
      };

/***********************************************************************************************************************
 *
 * Compute the media size to fit a needed size. It looks into the bluettePhotoSizes; it takes care of hidpi displays.
 *
 **********************************************************************************************************************/
    var computeMediaSize = function (neededSize)
      {
        var loadedSize = bluettePhotoSizes[0];

        $(bluettePhotoSizes).each(function(i, value)
          {
            if (neededSize * devicePixelRatio <= value)
              {
                loadedSize = 0 + value; // 0 + forces conversion to int
              }
          });

        debug("neededSize: %d, loadedSize: %d", neededSize, loadedSize);
        return loadedSize;
      };

/***********************************************************************************************************************
  *
  * Computes the largest fitting size of an image to be rendered within a container, preserving the aspect ratio.
  *
  **********************************************************************************************************************/
    var computeLargestFittingSize = function (component, container)
      {
        info("computeLargestFittingSize(%d x %d, %d x %d)",
             component.width, component.height, container.width, container.height);

        var border = 5; // FIXME

        var width = container.width - border * 2;
        var scale = Math.min(width / component.width, 1);

        if (component.height * scale > container.height)
          {
            height = container.height - border * 2;
            scale  = Math.min(height / component.height, 1);
          }

        var size =
          {
            width  : Math.round(component.width  * scale),
            height : Math.round(component.height * scale)
          };

        debug("returning %d x %d", size.width, size.height);

        return size;
      };

/***********************************************************************************************************************
 *
 * Notifies a fatal error.
 * FIXME: make it better
 *
 **********************************************************************************************************************/
    var fatal = function (message)
      {
        $("#initialLoadingWidget").hide();
        $("#loadingWidget").hide();
        $("#waitingWidget").hide();
        $("#content").append(message);
      };

/***********************************************************************************************************************
 *
 *
 **********************************************************************************************************************/
    function info (pattern, arg1, arg2, arg3, arg4)
      {
        if (bluetteLogging)
          {
            var d = new Date();
            console.log("%d:%d:%d.%d " + pattern, d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds(), arg1, arg2, arg3, arg4);
          }
      }

/***********************************************************************************************************************
 *
 *
 **********************************************************************************************************************/
    function debug (pattern, arg1, arg2, arg3, arg4)
      {
        if (bluetteLogging)
          {
            var d = new Date();
            console.log("%d:%d:%d.%d >>>> " + pattern, d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds(), arg1, arg2, arg3, arg4);
          }
      }

/***********************************************************************************************************************
 *
 *
 **********************************************************************************************************************/
    function defaultFor (arg, val)
      {
        return typeof arg !== 'undefined' ? arg : val;
      }

/***********************************************************************************************************************
 *
 * Initialization.
 *
 **********************************************************************************************************************/
    var bluetteCatalogUrl =                       defaultFor(window.bluetteCatalogUrl, "images.xml");
    var bluettePhotoPrefix =                     defaultFor(window.bluettePhotoPrefix, "please specify a URL");
    var bluetteHome =                                   defaultFor(window.bluetteHome, "/");
    var bluetteTitlePrefix =                     defaultFor(window.bluetteTitlePrefix, "Bluette Demo: ");
    var bluetteSlideshowSpeed =               defaultFor(window.bluetteSlideshowSpeed, 8000);
    var bluettePhotoSizes =                       defaultFor(window.bluettePhotoSizes, [1280, 800, 400, 200]);
    var bluetteThumbnailsPerRow =           defaultFor(window.bluetteThumbnailsPerRow, 10);
    var bluetteAvailWidthPercentage =   defaultFor(window.bluetteAvailWidthPercentage, 1.0);
    var bluetteAvailHeightPercentage = defaultFor(window.bluetteAvailHeightPercentage, 0.85);
    var bluetteUpdateUrl =                         defaultFor(window.bluetteUpdateUrl, false);
    var bluetteUpdateTitle =                     defaultFor(window.bluetteUpdateTitle, false);
    var bluetteShuffle =                             defaultFor(window.bluetteShuffle, false);
    var bluetteNavigation =                       defaultFor(window.bluetteNavigation, false);
    var bluetteTitleVisible =                   defaultFor(window.bluetteTitleVisible, false);
    var bluetteShowProgressIcons =         defaultFor(window.bluetteShowProgressIcons, true);
    var bluetteLogging =                             defaultFor(window.bluetteLogging, false);

    var devicePixelRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;

    var settings =
      {
        showArrows: true
      };

    var scrollPane = $('#thumbnails');
    scrollPane.jScrollPane(settings);
    var scrollPaneApi = scrollPane.data('jsp');

    var compatibilityUrl = location.href.replace(/#!\//, "#");
    var baseUrl = compatibilityUrl.replace(/#.*/, "");
    var initialStatus = compatibilityUrl.replace(/.*#/, "").replace(baseUrl, "").replace(/\/*\?.*$/, "");
    var autoPlay = initialStatus === "";
    var photos = [];
    var thumbnailsLoaded = false;
    var firstIndex = 0;

    var buttonsBound = false;

    var neededMediaSize = computeMediaSize(Math.max(screen.width * bluetteAvailWidthPercentage,
                                                    screen.height * bluetteAvailHeightPercentage));

    info("baseUrl: %s, initialStatus: '%s', devicePixelRatio: %d, neededMediaSize: %d",
         baseUrl, initialStatus, devicePixelRatio, neededMediaSize);

    setTimeout(loadCatalog, 0);
    $("#navigationCloseWidget").click(closeLightBox);
  });
