/*
 * Activates the JavaScript features for the Photo macro.
 *
 * Copyright (c) 2013-2025 by Fabrizio Giudici
 *
 */

$(document).ready(function()
  {
    $('.fancybox').fancybox(
      {
        nextEffect:  'fade',
        prevEffect:  'fade',

        nextClick:   true,

        /* Disable right click */
        beforeShow: function()
          {
            $.fancybox.wrap.bind("contextmenu", function (e)
              {
                return false;
              });
          },

        helpers:
          {
            title:
              {
                type: 'inside'
              }
          }
      });

    $(".framedPhotoWithLoupe").elevateZoom(
      {
        zoomType:        "lens",
        lensShape:       "square",
        lensSize:         300,
        lensFadeIn:       300,
        lensFadeOut:      300,
        borderSize:       8,
        loadingIcon:     '/media/fancyBox-2.1.3/fancybox_loading.gif',
        onFgZoomedImageLoading: function(id)
          {
            $("#" + id + "_Overlay").text("Loading 1:1 image... loupe not available yet");
          },
        onFgZoomedImageLoaded: function(id)
          {
            $("#" + id + "_Overlay").text("1:1 loupe available");
          }
      });

    $('div.content').on('click', '.loupeLink', function(e)
      {
        e.preventDefault();
        id     = $(this).attr('loupeId');
        loupeX = parseFloat($(this).attr('loupeX'));
        loupeY = parseFloat($(this).attr('loupeY'));
        ez     = $('#' + id).data('elevateZoom');
        ez.setElements("hide");
        ez.setFgPercentPosition(loupeX, loupeY);
        ez.setElements("show");
      });
  });

