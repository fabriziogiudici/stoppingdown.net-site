/*
 * Activates the JavaScript features for the PhotoComparatorWithLoupe macro.
 *
 * Copyright (c) 2013-2014 by Fabrizio Giudici
 *
 */

$(document).ready(function()
  {
// WITHOUT LOUPE

    $('.photoComparatorSelector').on('change', function()
      {
        var select = $(this);
        var groupId = $(select).attr('groupId');
        var val = $(select).val();
        var leftId = val.replace(/@@@.*$/, "");
        var rightId = val.replace(/^.*@@@/, "");;

        var mediaBasePath = $(select).attr('mediaBasePath');
        var leftImage = "/media" + mediaBasePath + "/" + leftId + ".jpg";
        var rightImage = "/media" + mediaBasePath + "/" + rightId + ".jpg";

        var leftEz = $('#left_' + groupId).data('elevateZoom');
        var rightEz = $('#right_' + groupId).data('elevateZoom');
        $(leftEz).data("imageId", leftId);
        $(rightEz).data("imageId", rightId);
        $('#leftLabel_' + groupId).text("Loading...");
        $('#rightLabel_' + groupId).text("Loading...");
        leftEz.swaptheimage(leftImage, leftImage);
        rightEz.swaptheimage(rightImage, rightImage);
      });

    $(".photo-comparator").elevateZoom(
      {
        zoomEnabled:         false,
        borderSize:          0,
        loadingIcon:         '/bluette-1.3/graphics/linear-spinner.gif',
        onImageSwapComplete: function notifyImageLoaded(element)
          {
            var ez = $(element).data('elevateZoom');
            var id = $(element).attr("id");
            var photoId = $(ez).data("imageId");
            var labelId = id.replace("_", "Label_");
            $('#' + labelId).text(photoId);
          }
      });

// WITH LOUPE

    function changeImage(side)
      {
        var image = $('#' + side + 'Selector').val();
        var mode  = $('#' + side + 'ModeSelector').val();

        var mediaBasePath = $('#' + side + 'Selector').attr('mediaBasePath');
        var smallImage = "/media" + mediaBasePath + "/" + image + mode + "-small.jpg";
        var largeImage = "/media" + mediaBasePath + "/" + image + mode + ".jpg";

        var ez = $('#' + side + 'Image').data('elevateZoom');

        if (ez !== null)
          {
            ez.swaptheimage(smallImage, largeImage);
          }
      }

    $('#leftSelector').on('change', function()
      {
        changeImage('left');
      });

    $('#leftModeSelector').on('change', function()
      {
        changeImage('left');
      });

    $('#rightSelector').on('change', function()
      {
        changeImage('right');
      });

    $('#rightModeSelector').on('change', function()
      {
        changeImage('right');
      });

    $('#lock').on('change', function()
      {
        // TODO: if checked, move right loupe
      });

    $('.comparer').on('click', '#resetLoupeZoom', function(e)
      {
        leftEz  = $('#leftImage').data('elevateZoom');
        rightEz = $('#rightImage').data('elevateZoom');
        leftEz.setElements("show");
        rightEz.setElements("show");
        leftEz.changeZoomLevel(1);
        rightEz.changeZoomLevel(1);
      });

    $('#quickSelectors').on('click', '.quickSelector', function(e)
      {
        e.preventDefault();
        leftImage = $(this).attr('leftImage');
        rightImage = $(this).attr('rightImage');
        leftX   = parseFloat($(this).attr('leftX'));
        rightX  = parseFloat($(this).attr('rightX'));
        leftY   = parseFloat($(this).attr('leftY'));
        rightY  = parseFloat($(this).attr('rightY'));
        leftEz  = $('#leftImage').data('elevateZoom');
        rightEz = $('#rightImage').data('elevateZoom');
        leftEz.changeZoomLevel(1);
        rightEz.changeZoomLevel(1);
        $("#leftSelector").val(leftImage);
        $("#rightSelector").val(rightImage);
        $("#leftModeSelector").val('Raw');
        $("#rightModeSelector").val('Raw');
        $('#lock').prop('checked', false);
        changeImage('left');
        changeImage('right');
        leftEz.setFgPercentPosition(leftX, leftY);
        rightEz.setFgPercentPosition(rightX, rightY);
      });

    $("#leftImage").elevateZoom(
      {
        cursor:              "crosshair",
        scrollZoom:          true,
        tint:                true,
        tintColour:          '#000',
        tintOpacity:         0.5,
        zoomWindowPosition:  "leftZoom",
        zoomWindowWidth:     500,
        zoomWindowHeight:    360,
        borderSize:          0,
        fgHideZoomOnExit:    false,
        loadingIcon:         '/bluette-1.3/graphics/linear-spinner.gif',
        onFgNotifyZoomLevel: function(value, percent)
          {
            $('#leftZoomLevel').text(percent);
            var ez = $('#rightImage').data('elevateZoom');
            ez.changeZoomLevel(value);
          },
        onFgNotifyPosition:  function(xPercent, yPercent)
          {
            var lock = $('#lock').attr('checked');

            if (lock === 'checked')
              {
                var ez = $('#rightImage').data('elevateZoom');
                ez.setFgPercentPosition(xPercent, yPercent);
              }
          }
      });

    $("#rightImage").elevateZoom(
      {
        cursor:              "crosshair",
        scrollZoom:          true,
        tint:                true,
        tintColour:          '#000',
        tintOpacity:         0.5,
        zoomWindowPosition:  "rightZoom",
        zoomWindowWidth:     500,
        zoomWindowHeight:    360,
        borderSize:          0,
        fgHideZoomOnExit:    false,
        loadingIcon:         '/bluette-1.3/graphics/linear-spinner.gif',
        onFgNotifyZoomLevel: function(value, percent)
          {
            $('#rightZoomLevel').text(percent);
            var ez = $('#leftImage').data('elevateZoom');
            //ez.changeZoomLevel(value);
          }
      });

    changeImage('left');
    changeImage('right');
});
