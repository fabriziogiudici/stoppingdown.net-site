<!--
    Renders a photo as a thumbnail to be placed in a grid.

    It uses a fixed size of 800 pixels, since the thumbnail grid changes with the screen size, and it's too complex to
    accurately compute the required size. It also enforces a 3:2 aspect ratio (eventually stretching the images).
    This means that all the photos used must follow that ratio - if there are non-neglectable departures from this
    ratio, some stretching will be visible.
    Imposing the same height is needed to avoid gaps in the matrix, that can be caused by photos taller than others,
    which 'eat' space from the underlying cell.
-->
<xsl:template match="div[@class='nwXsltMacro_Thumbnail']">
    <xsl:variable name="id">
        <xsl:value-of select="p[@class='nwXsltMacro_Thumbnail_image']"/>
    </xsl:variable>
    <xsl:variable name="image">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/800/image.jpg')$</xsl:variable>
    <xsl:element name="div">
        <xsl:attribute name="class">col-xs-6 col-md-4</xsl:attribute>
        <xsl:element name="div">
            <xsl:attribute name="class">thumbnail</xsl:attribute>
            <xsl:element name="a">
                <xsl:attribute name="href">
                    <xsl:value-of select="p[@class='nwXsltMacro_Thumbnail_link']"/>
                </xsl:attribute>
                <xsl:call-template name="single_res_photo">
                    <xsl:with-param name="id" select="$id"/>
                    <xsl:with-param name="size" select="800"/>
                    <xsl:with-param name="extraClasses" select="'visible aspect-ratio-3_2'"/>
                    <xsl:with-param name="placeHolderSize" select="0"/>
                </xsl:call-template>
                <xsl:element name="div">
                    <xsl:attribute name="class">caption</xsl:attribute>
                    <xsl:element name="h3">
                        <xsl:value-of select="p[@class='nwXsltMacro_Thumbnail_label']"/>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>
