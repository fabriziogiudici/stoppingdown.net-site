
<xsl:template match="div[@class='nwXsltMacro_Movie']">
    <xsl:element name="div" namespace="">
        <xsl:attribute name="class">article-movie</xsl:attribute>
        <!-- <xsl:element name="object">
            <xsl:attribute name="width"><xsl:value-of select="p[@class='nwXsltMacro_Movie_width']"/></xsl:attribute>
            <xsl:attribute name="height"><xsl:value-of select="p[@class='nwXsltMacro_Movie_height']"/></xsl:attribute>
            <xsl:attribute name="classid">clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B</xsl:attribute>
            <xsl:attribute name="codebase">http://www.apple.com/qtactivex/qtplugin.cab</xsl:attribute>            
            <xsl:element name="param">
                <xsl:attribute name="name">src</xsl:attribute>
                <xsl:attribute name="value">$mediaLink(relativePath='/movies/<xsl:value-of select="p[@class='nwXsltMacro_Movie_movieId']"/>.mov')$</xsl:attribute>
            </xsl:element>
            <xsl:element name="param">
                <xsl:attribute name="name">autoplay</xsl:attribute>
                <xsl:attribute name="value">false</xsl:attribute>
            </xsl:element>
            <xsl:element name="param">
                <xsl:attribute name="name">autohref</xsl:attribute>
                <xsl:attribute name="value">false</xsl:attribute>
            </xsl:element>
            <xsl:element name="param">
                <xsl:attribute name="name">controller</xsl:attribute>
                <xsl:attribute name="value">true</xsl:attribute>
            </xsl:element>
            <xsl:element name="param">
                <xsl:attribute name="name">loop</xsl:attribute>
                <xsl:attribute name="value">true</xsl:attribute>
            </xsl:element>
            <xsl:element name="embed">
                <xsl:attribute name="width"><xsl:value-of select="p[@class='nwXsltMacro_Movie_width']"/></xsl:attribute>
                <xsl:attribute name="height"><xsl:value-of select="p[@class='nwXsltMacro_Movie_height']"/></xsl:attribute>
                <xsl:attribute name="src">$mediaLink(relativePath='/movies/<xsl:value-of select="p[@class='nwXsltMacro_Movie_movieId']"/>.mov')$</xsl:attribute>
                <xsl:attribute name="autoplay">false</xsl:attribute>
                <xsl:attribute name="autohref">false</xsl:attribute>
                <xsl:attribute name="controller">true</xsl:attribute>
                <xsl:attribute name="loop">true</xsl:attribute>
                <xsl:attribute name="pluginspage">http://www.apple.com/quicktime/download/</xsl:attribute>
            </xsl:element>
        </xsl:element>-->
        
        <xsl:element name="div">
            <xsl:element name="video">
                <xsl:attribute name="width"><xsl:value-of select="p[@class='nwXsltMacro_Movie_width']"/></xsl:attribute>
                <xsl:attribute name="height"><xsl:value-of select="p[@class='nwXsltMacro_Movie_height']"/></xsl:attribute>
                <xsl:attribute name="src">$mediaLink(relativePath='/movies/<xsl:value-of select="p[@class='nwXsltMacro_Movie_movieId']"/>/800/movie.mp4')$</xsl:attribute>
                <xsl:attribute name="preload">preload</xsl:attribute>
                <xsl:attribute name="controls">controls</xsl:attribute>
                <xsl:attribute name="loop">loop</xsl:attribute>
            </xsl:element>
        </xsl:element>
        
        <xsl:element name="p">
            <xsl:value-of select="p[@class='nwXsltMacro_Movie_caption']"/>
        </xsl:element>
    </xsl:element>
</xsl:template>
