<!--
This script defines the following macros:

    nwXsltMacro_Photo:                  renders a single photo
    nwXsltMacro_PhotoTabs:              renders multiple photos selectable with tabs
    nwXsltMacro_PhotoPair:              renders two photos side-by-side
    nwXsltMacro_PhotoComparator:        renders two photos side-by-side picking from a set of pairs
    nwXsltMacro_InlinedPhoto:           seems unused.
-->

<!--
Renders a single photo. Parameters:

    nwXsltMacro_Photo_photoId:          the id of the photo to render.
    nwXsltMacro_Photo_caption:          ?
    nwXsltMacro_Photo_title:            the title of the photo to use as a caption.
    nwXsltMacro_Photo_shootingData:     the shooting data.
    nwXsltMacro_Photo_zoomType:         none or 'loupe' or 'loupe+popup'; the former doesn't include the photo in the fancyBox collection.

Example for rendering a photo with title and shooting data retrieved from metadata:

    <div class="nwXsltMacro_Photo">
        <p class="nwXsltMacro_Photo_photoId">20240828-0360</p>
        <p class="nwXsltMacro_Photo_title">$IPTC.IIM.title$</p>
        <p class="nwXsltMacro_Photo_shootingData">$shootingData$, slightly cropped.</p>
    </div>

Example for activating a loupe when hovering on the photo:

    <div class="nwXsltMacro_Photo">
        <p class="nwXsltMacro_Photo_photoId">20131101-0032</p>
        <p class="nwXsltMacro_Photo_title">$IPTC.IIM.title$</p>
        <p class="nwXsltMacro_Photo_shootingData">$shootingData$, monopod.</p>
        <p class="nwXsltMacro_Photo_zoomType">loupe+popup</p>
    </div>

The loupe can be activated by an HTML anchor with the following syntax:

    <a class="loupeLink" href="#" loupeid="20131227-0031" loupex="0.02" loupey="0.72">building windows</a>

The attribute loupeId selects one among multiple photos in the page and coordinates are in the range [0,1].

-->
<xsl:template match="div[@class='nwXsltMacro_Photo']">
    <xsl:element name="div" namespace="">
        <xsl:attribute name="class">article-photo</xsl:attribute>
        <xsl:call-template name="render_photo">
            <xsl:with-param name="id" select="p[@class='nwXsltMacro_Photo_photoId']"/>
            <xsl:with-param name="caption" select="p[@class='nwXsltMacro_Photo_caption']"/>
            <xsl:with-param name="titleFormat" select="p[@class='nwXsltMacro_Photo_title']"/>
            <xsl:with-param name="shootingDataFormat" select="p[@class='nwXsltMacro_Photo_shootingData']"/>
            <xsl:with-param name="zoomType" select="p[@class='nwXsltMacro_Photo_zoomType']"/>
        </xsl:call-template>
    </xsl:element>
</xsl:template>

<!--
Renders a set of photos with selection tabs above. Parameters:

    nwXsltMacro_PhotoTabs_id:           the id of the group of photos; must be unique in a page.
    nwXsltMacro_PhotoTabs_label:        the label of a selection tab.
    nwXsltMacro_PhotoTabs_photoId:      the id of the photo to render in a selection tab.
    nwXsltMacro_PhotoTabs_active:       'true' if the tab must be rendered by default.

Example:

    <div class="nwXsltMacro_PhotoTabs">
        <p class="nwXsltMacro_PhotoTabs_id">bokeh</p>
        <table>
            <tr>
                <td class="nwXsltMacro_PhotoTabs_label">ƒ/1.8</td>
                <td class="nwXsltMacro_PhotoTabs_photoId">20230925-0168</td>
                <td class="nwXsltMacro_PhotoTabs_active">true</td>
            </tr>
            <tr>
                <td class="nwXsltMacro_PhotoTabs_label">ƒ/2.5</td>
                <td class="nwXsltMacro_PhotoTabs_photoId">20230925-0176</td>
            </tr>
            <tr>
                <td class="nwXsltMacro_PhotoTabs_label">ƒ/4</td>
                <td class="nwXsltMacro_PhotoTabs_photoId">20230925-0180</td>
            </tr>
        </table>
    </div>
-->
<xsl:template match="div[@class='nwXsltMacro_PhotoTabs']">
    <xsl:variable name="id">
        <xsl:value-of select="p[@class='nwXsltMacro_PhotoTabs_id']"/>
    </xsl:variable>
    <xsl:element name="div" namespace="">
        <xsl:attribute name="class">photoTabs</xsl:attribute>
        <xsl:element name="ul">
            <xsl:attribute name="class">nav nav-tabs</xsl:attribute>
            <xsl:for-each select="table/tr">
                <xsl:element name="li">
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:value-of select="'#'"/><xsl:value-of select="$id"/>-<xsl:value-of select="td[@class='nwXsltMacro_PhotoTabs_photoId']"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-toggle">tab</xsl:attribute>
                        <xsl:value-of select="td[@class='nwXsltMacro_PhotoTabs_label']"/>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
        <xsl:element name="div">
            <xsl:attribute name="class">tab-content</xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="$id"/></xsl:attribute>
            <xsl:for-each select="table/tr">
                <xsl:element name="div" namespace="">
                    <xsl:attribute name="class">
                        <xsl:choose>
                            <xsl:when test="td[@class='nwXsltMacro_PhotoTabs_active'] = 'true'">
                                <xsl:value-of select="'tab-pane in active'"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'tab-pane'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="id">
                        <xsl:value-of select="$id"/>-<xsl:value-of select="td[@class='nwXsltMacro_PhotoTabs_photoId']"/>
                    </xsl:attribute>
                    <xsl:element name="div" namespace="">
                        <xsl:attribute name="class">article-photo</xsl:attribute>
                        <xsl:call-template name="render_photo">
                            <xsl:with-param name="id" select="td[@class='nwXsltMacro_PhotoTabs_photoId']"/>
                            <xsl:with-param name="shootingDataFormat" select="'$shootingData$.'"/>
                        </xsl:call-template>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!--
Renders a pair of photos side-by-side. Parameters:

    nwXsltMacro_PhotoPair_photo1Id:         the id of the former photo to render.
    nwXsltMacro_PhotoPair_photo2Id:         the id of the latter photo to render.
    nwXsltMacro_PhotoPair_title1:           the caption of the former photo.
    nwXsltMacro_PhotoPair_title2:           the caption of the latter photo.
    nwXsltMacro_PhotoPair_shootingData1:    the shooting data of the former photo.
    nwXsltMacro_PhotoPair_shootingData2:    the shooting data of the latter photo.

Example:

    <div class="nwXsltMacro_PhotoPair">
        <p class="nwXsltMacro_PhotoPair_photo1Id">20041025-0097</p>
        <p class="nwXsltMacro_PhotoPair_photo2Id">20041025-0098</p>
        <p class="nwXsltMacro_PhotoPair_shootingData1">$shootingData$, flash + sunlight</p>
        <p class="nwXsltMacro_PhotoPair_shootingData1">$shootingData$, flash</p>
    </div>
-->
<xsl:template match="div[@class='nwXsltMacro_PhotoPair']">
    <xsl:variable name="title1" select="p[@class='nwXsltMacro_PhotoPair_title1']"/>
    <xsl:variable name="title2" select="p[@class='nwXsltMacro_PhotoPair_title2']"/>
    <xsl:element name="div" namespace="">
        <xsl:attribute name="class">article-photo</xsl:attribute>
        <xsl:element name="div" namespace="">
            <xsl:attribute name="class">photoPair</xsl:attribute>
            <xsl:element name="div" namespace="">
                <xsl:call-template name="render_photo">
                    <xsl:with-param name="id" select="p[@class='nwXsltMacro_PhotoPair_photo1Id']"/>
                    <xsl:with-param name="shootingDataFormat" select="p[@class='nwXsltMacro_PhotoPair_shootingData1']"/>
                    <xsl:with-param name="titleFormat" select="p[@class='nwXsltMacro_PhotoPair_title1']"/>
                </xsl:call-template>
            </xsl:element>
            <xsl:element name="div" namespace="">
                <xsl:element name="span" namespace="">
                </xsl:element>
            </xsl:element>
            <xsl:element name="div" namespace="">
                <xsl:call-template name="render_photo">
                    <xsl:with-param name="id" select="p[@class='nwXsltMacro_PhotoPair_photo2Id']"/>
                    <xsl:with-param name="shootingDataFormat" select="p[@class='nwXsltMacro_PhotoPair_shootingData2']"/>
                    <xsl:with-param name="titleFormat" select="p[@class='nwXsltMacro_PhotoPair_title2']"/>
                </xsl:call-template>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!--
Renders two photos side-by-side picking from a set of pairs by means of a drop-down menu. Parameters:

    nwXsltMacro_PhotoComparator_groupId:            a unique id in the page
    nwXsltMacro_PhotoComparator_mediaBasePath:      the base path where photos have to be retrieved
    nwXsltMacro_PhotoComparator_options:            a table with a label and a pair of ids of the photos

Example:

    <div class="nwXsltMacro_PhotoComparator">
        <p class="nwXsltMacro_PhotoComparator_groupId">24</p>
        <p class="nwXsltMacro_PhotoComparator_mediaBasePath">/testimages/20140112</p>
        <table class="nwXsltMacro_PhotoComparator_options">
            <tr>
                <td>S1670-24l</td>
                <td>S1670-24r</td>
                <td>Sony Zeiss Vario-Tessar T* E 16-70mm F4 ZA OSS border comparison</td>
            </tr>
            <tr>
                <td>S1670-24l</td>
                <td>S1670-24c</td>
                <td>Sony Zeiss Vario-Tessar T* E 16-70mm F4 ZA OSS left border vs centre comparison</td>
            </tr>
            <tr>
                <td>S1670-24c</td>
                <td>S1670-24r</td>
                <td>Sony Zeiss Vario-Tessar T* E 16-70mm F4 ZA OSS centre vs right border comparison</td>
            </tr>
        </table>
    </div>
-->
<xsl:template match="div[@class='nwXsltMacro_PhotoComparator']">
    <xsl:variable name="mediaBasePath" select="p[@class='nwXsltMacro_PhotoComparator_mediaBasePath']"/>
    <xsl:variable name="groupId" select="p[@class='nwXsltMacro_PhotoComparator_groupId']"/>
    <xsl:variable name="firstId1" select="table[@class='nwXsltMacro_PhotoComparator_options']/tr[1]/td[1]|table[@class='nwXsltMacro_PhotoComparator_options']/tbody/tr[1]/td[1]"/>
    <xsl:variable name="firstId2" select="table[@class='nwXsltMacro_PhotoComparator_options']/tr[1]/td[2]|table[@class='nwXsltMacro_PhotoComparator_options']/tbody/tr[1]/td[2]"/>

    <xsl:element name="div" namespace="">
        <xsl:attribute name="class">comparer</xsl:attribute>
        <xsl:element name="table" namespace="">
            <xsl:element name="tbody" namespace="">
                <xsl:element name="tr" namespace="">
                    <xsl:element name="td" namespace="">
                        <xsl:attribute name="colspan">2</xsl:attribute>
                        <xsl:element name="select" namespace="">
                            <xsl:attribute name="id">selector_<xsl:value-of select="$groupId"/></xsl:attribute>
                            <xsl:attribute name="class">photoComparatorSelector</xsl:attribute>
                            <xsl:attribute name="mediaBasePath">
                                <xsl:value-of select="$mediaBasePath"/>
                            </xsl:attribute>
                            <xsl:attribute name="groupId"><xsl:value-of select="$groupId"/></xsl:attribute>
                            <xsl:for-each select="table[@class='nwXsltMacro_PhotoComparator_options']/tr|table[@class='nwXsltMacro_PhotoComparator_options']/tbody/tr">
                                <xsl:element name="option" namespace="">
                                    <xsl:attribute name="value"><xsl:value-of select="td[1]"/>@@@<xsl:value-of select="td[2]"/></xsl:attribute>
                                    <xsl:text>
                                        <xsl:value-of select="td[3]"/>
                                    </xsl:text>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="tr" namespace="">
                    <xsl:element name="td" namespace="">
                        <xsl:element name="div" namespace="">
                            <xsl:attribute name="class">comparator-left</xsl:attribute>
                            <xsl:element name="img">
                                <xsl:attribute name="class">photo-comparator</xsl:attribute>
                                <xsl:attribute name="id">left_<xsl:value-of select="$groupId"/></xsl:attribute>
                                <xsl:attribute name="src">$mediaLink(relativePath='<xsl:value-of select="$mediaBasePath"/>/<xsl:value-of select="$firstId1"/>.jpg')$</xsl:attribute>
                                <xsl:attribute name="alt"><xsl:value-of select="$firstId1"/></xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="td" namespace="">
                        <xsl:element name="div" namespace="">
                            <xsl:attribute name="class">comparator-right</xsl:attribute>
                            <xsl:element name="img">
                                <xsl:attribute name="class">photo-comparator</xsl:attribute>
                                <xsl:attribute name="id">right_<xsl:value-of select="$groupId"/></xsl:attribute>
                                <xsl:attribute name="src">$mediaLink(relativePath='<xsl:value-of select="$mediaBasePath"/>/<xsl:value-of select="$firstId2"/>.jpg')$</xsl:attribute>
                                <xsl:attribute name="alt"><xsl:value-of select="$firstId2"/></xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="tr" namespace="">
                    <xsl:element name="td" namespace="">
                        <xsl:element name="span">
                            <xsl:attribute name="id">leftLabel_<xsl:value-of select="$groupId"/></xsl:attribute>
                            <xsl:text><xsl:value-of select="$firstId1"/></xsl:text>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="td" namespace="">
                        <xsl:element name="span">
                            <xsl:attribute name="id">rightLabel_<xsl:value-of select="$groupId"/></xsl:attribute>
                            <xsl:text><xsl:value-of select="$firstId2"/></xsl:text>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!--
    Seems unused.
-->
<xsl:template match="span[@class='nwXsltMacro_InlinedPhoto']">
    <xsl:variable name="id" select="span[@class='nwXsltMacro_InlinedPhoto_photoId']"/>
    <xsl:variable name="caption" select="span[@class='nwXsltMacro_InlinedPhoto_caption']"/>
    <xsl:element name="a">
        <xsl:attribute name="class">fancybox</xsl:attribute>
        <xsl:attribute name="href">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/3840/image.jpg')$</xsl:attribute>
        <xsl:attribute name="title">
            <xsl:value-of select="$caption"/>
        </xsl:attribute>
        <xsl:element name="img">
            <xsl:attribute name="class">inlined-photo-<xsl:value-of select="span[@class='nwXsltMacro_InlinedPhoto_float']"/></xsl:attribute>
            <xsl:attribute name="src">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/300/image.jpg')$</xsl:attribute>
            <xsl:attribute name="alt"></xsl:attribute>
        </xsl:element>
    </xsl:element>
    <xsl:element name="span">
        <xsl:attribute name="class">inlined-photo-caption</xsl:attribute>
        <xsl:value-of select="$caption"/>
    </xsl:element>
</xsl:template>

<!--
    Renders a single photo.
-->
<xsl:template name="render_photo">
    <xsl:param name="id"/>
    <xsl:param name="caption"/>
    <xsl:param name="titleFormat"/>
    <xsl:param name="title" select="java:it.tidalwave.northernwind.frontend.ui.component.gallery.spi.MediaMetadataXsltAdapter.getMetadataString($id, $titleFormat)"/>
    <xsl:param name="shootingDataFormat" select="p[@class='nwXsltMacro_Photo_shootingData']"/>
    <xsl:param name="shootingData" select="java:it.tidalwave.northernwind.frontend.ui.component.gallery.spi.MediaMetadataXsltAdapter.getMetadataString($id, $shootingDataFormat)"/>
    <xsl:param name="zoomType" select="p[@class='nwXsltMacro_Photo_zoomType']"/>
    <!-- FIXME: 1920, 3840 and full are hardwired -->
    <xsl:param name="image1920">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/1920/image.jpg')$</xsl:param>
    <xsl:param name="image3840">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/3840/image.jpg')$</xsl:param>
    <xsl:param name="imageFull">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/full/image.jpg')$</xsl:param>
    <xsl:element name="a" namespace="">
        <xsl:attribute name="anchor">
            <xsl:value-of select="$id"/>
        </xsl:attribute>
    </xsl:element>
    <!-- div for printing -->
    <xsl:element name="div" namespace="">
        <xsl:attribute name="style">
            <xsl:value-of select="'display: none'"/>
        </xsl:attribute>
        <xsl:attribute name="class">
            <xsl:value-of select="'printOnly'"/>
        </xsl:attribute>
        <xsl:element name="img">
            <xsl:attribute name="class">framed</xsl:attribute>
            <xsl:attribute name="src">
                <xsl:value-of select="$image1920"/>
            </xsl:attribute>
        </xsl:element>
    </xsl:element>
    <!-- figure -->
    <xsl:element name="figure">
        <xsl:element name="div">
            <xsl:choose>
                <!-- The download link and information above the photo. -->
                <xsl:when test="($zoomType = 'loupe') or ($zoomType = 'loupe+popup')">
                    <xsl:element name="div">
                        <xsl:attribute name="class">article-photo-overlay</xsl:attribute>
                        <xsl:element name="a">
                            <xsl:attribute name="href">
                                <xsl:value-of select="$imageFull"/>
                            </xsl:attribute>
                            <xsl:text>Download full-size image</xsl:text>
                        </xsl:element>
                        <xsl:element name="span">
                            <xsl:text> — </xsl:text>
                        </xsl:element>
                        <xsl:element name="span">
                            <xsl:attribute name="id">
                                <xsl:value-of select="$id"/>_Overlay</xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                </xsl:when>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="$zoomType = 'loupe'">
                    <xsl:element name="img">
                        <xsl:attribute name="id">
                            <xsl:value-of select="$id"/>
                        </xsl:attribute>
                        <xsl:attribute name="class">framedPhotoWithLoupe</xsl:attribute>
                        <xsl:attribute name="src">
                            <xsl:value-of select="$image1920"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-zoom-image">
                            <xsl:value-of select="$imageFull"/>
                        </xsl:attribute>
                    </xsl:element>
                </xsl:when>
                <xsl:when test="$zoomType = 'loupe+popup'">
                    <xsl:element name="a">
                        <xsl:attribute name="class">fancybox</xsl:attribute>
                        <xsl:attribute name="href">
                            <xsl:value-of select="$image3840"/>
                        </xsl:attribute>
                        <xsl:attribute name="title">
                            <xsl:value-of select="$title"/>
                        </xsl:attribute>
                        <xsl:attribute name="rel">page-gallery</xsl:attribute>
                        <xsl:element name="img">
                            <xsl:attribute name="id">
                                <xsl:value-of select="$id"/>
                            </xsl:attribute>
                            <xsl:attribute name="class">framedPhotoWithLoupe</xsl:attribute>
                            <xsl:attribute name="src">
                                <xsl:value-of select="$image1920"/>
                            </xsl:attribute>
                            <xsl:attribute name="data-zoom-image">
                                <xsl:value-of select="$imageFull"/>
                            </xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name="a">
                        <xsl:attribute name="class">fancybox</xsl:attribute>
                        <xsl:attribute name="href">
                            <xsl:value-of select="$image3840"/>
                        </xsl:attribute>
                        <xsl:attribute name="title">
                            <xsl:value-of select="$title"/>
                        </xsl:attribute>
                        <xsl:attribute name="rel">page-gallery</xsl:attribute>
                        <xsl:call-template name="all_res_photo">
                            <xsl:with-param name="id" select="$id"/>
                        </xsl:call-template>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
        <!-- figCaption (must be the last direct child of figure) -->
        <xsl:if test="$caption or $title or $shootingData">
            <xsl:element name="figCaption">
                <xsl:if test="$shootingData">
                    <xsl:element name="p">
                        <xsl:attribute name="class">shootingData</xsl:attribute>
                        <xsl:value-of select="$shootingData"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test="$caption">
                    <xsl:element name="p">
                        <xsl:attribute name="class">caption</xsl:attribute>
                        <xsl:value-of select="$caption"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test="$title">
                    <xsl:element name="p">
                        <xsl:attribute name="class">title</xsl:attribute>
                        <xsl:value-of select="$title"/>
                    </xsl:element>
                </xsl:if>
            </xsl:element>
        </xsl:if>
    </xsl:element>
</xsl:template>

<!--
    Renders a single photo with optimised variants for resolution.
    See http://timkadlec.com/2012/04/media-query-asset-downloading-results/
-->
<xsl:template name="all_res_photo">
    <xsl:param name="id"/>
    <xsl:variable name="image200">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/200/image.jpg')$</xsl:variable>
    <xsl:element name="div">
        <xsl:attribute name="class">framedPhoto-container size200</xsl:attribute>
        <xsl:element name="div">
            <xsl:attribute name="style">background-image:url('<xsl:value-of select="$image200"/>')</xsl:attribute>
            <xsl:element name="img">
                <xsl:attribute name="class">placeholder</xsl:attribute>
                <xsl:attribute name="src">
                    <xsl:value-of select="$image200"/>
                </xsl:attribute>
                <xsl:attribute name="alt"></xsl:attribute>
            </xsl:element>
        </xsl:element>
    </xsl:element>
    <xsl:call-template name="single_res_photo">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="size" select="200"/>
        <xsl:with-param name="extraClasses" select="'size200'"/>
    </xsl:call-template>
    <xsl:call-template name="single_res_photo">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="size" select="400"/>
        <xsl:with-param name="extraClasses" select="'size400'"/>
    </xsl:call-template>
    <xsl:call-template name="single_res_photo">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="size" select="800"/>
        <xsl:with-param name="extraClasses" select="'size800'"/>
    </xsl:call-template>
    <xsl:call-template name="single_res_photo">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="size" select="1920"/>
        <xsl:with-param name="extraClasses" select="'size1920'"/>
    </xsl:call-template>
    <xsl:call-template name="single_res_photo">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="size" select="3840"/>
        <xsl:with-param name="extraClasses" select="'size3840'"/>
    </xsl:call-template>
</xsl:template>

<!--
    Renders a single photo with additional layers for a 'loading' icon and a default background.
    In order to support responsive web design, <img> is not used, since it always loads its resource.
    Instead a <div> with a background image is used. In this way multiple <div>s can be used, with different
    image sizes, and only one can be made visible by means of CSS.

    id:             the id of the photo
    size:           the size of the photo
    extraClasses    the extra CSS classes to use. By default, 'visibile' which makes the object visible by default.
    placeHolderSize if > 0, the same image with the given size will be used, invisible, to lay out the dimensions of
                    the <div>; in particular, since the width is usually set by the markup, to compute the height.
                    Passing 0 implies that the layout must be entirely set by the markup.
-->
<xsl:template name="single_res_photo">
    <xsl:param name="id"/>
    <xsl:param name="size"/>
    <xsl:param name="extraClasses" select="'visible'"/>
    <xsl:param name="placeHolderSize" select="'200'"/>
    <xsl:variable name="placeholder">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/<xsl:value-of select="$placeHolderSize"/>/image.jpg')$</xsl:variable>
    <xsl:variable name="hiresImage">$mediaLink(relativePath='/stillimages/<xsl:value-of select="$id"/>/<xsl:value-of select="$size"/>/image.jpg')$</xsl:variable>
    <xsl:element name="div">
        <xsl:attribute name="class">framedPhoto-container <xsl:value-of select="$extraClasses"/></xsl:attribute>
        <xsl:if test="$placeHolderSize > 0">
            <xsl:element name="img">
                <xsl:attribute name="class">placeholder</xsl:attribute>
                <xsl:attribute name="src">
                    <xsl:value-of select="$placeholder"/>
                </xsl:attribute>
                <xsl:attribute name="alt"></xsl:attribute>
            </xsl:element>
        </xsl:if>
        <xsl:element name="div">
            <xsl:attribute name="class">background</xsl:attribute>
            <xsl:text> </xsl:text>
        </xsl:element>
        <xsl:element name="div">
            <xsl:attribute name="class">loading</xsl:attribute>
            <xsl:text> </xsl:text>
        </xsl:element>
        <xsl:element name="div">
            <xsl:attribute name="class">image</xsl:attribute>
            <xsl:attribute name="style">background-image:url('<xsl:value-of select="$hiresImage"/>')</xsl:attribute>
            <xsl:text> </xsl:text>
        </xsl:element>
    </xsl:element>
</xsl:template>
