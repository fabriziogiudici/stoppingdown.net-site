<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" media="screen" href="../../../library/css/editing.css" type="text/css" />
  </head>
  <body>
    <p class="first">As you know from <a href="/blog/breaking-the-mirror/">my previous
        blog post</a>, I've ordered a <a href="http://store.sony.com/alpha-nex-6/cat-27-catid-All-Alpha-NEX-6-Cameras">Sony
        NEX-6</a>. It arrived yesterday with my first lens, a <a href="http://www.sigmaphoto.com/product/30mm-f28-ex-dn">Sigma
        30mm F2.8 DN | A</a>, actually earlier than expected (the carriers were very efficient). Unfortunately the
      weather is being bad and will be apparently still bad in the weekend; but today I was able to leave the office in
      the afternoon and, while driving home, the sun appeared for about one hour, offering a decent light. I didn't have
      either the tripod or the monopod in my car, but the scenario was good enough for taking a few first hand held
      shots.</p>
    <p>What you see below is my very first photo with the NEX-6 (with the exception of some random shots of the previous
      evening just to get used with the dials and menus).</p>
    <p><em>Note that all the photos in this post have been post-processed with Adobe Lightroom as per my usual workflow:
        curves, saturation, sharpening, cropping / rotation / perspective correction and lens defect correction. <br />
      </em></p>
    <div class="nwXsltMacro_Photo">
      <p class="nwXsltMacro_Photo_photoId">20131025-0001</p>
      <p class="nwXsltMacro_Photo_title">$IPTC.IIM.title$</p>
      <p class="nwXsltMacro_Photo_shootingData">$shootingData$</p>
    </div>
    <p>I have to say that initially I wasn't really at ease: after thirteen years of exclusive use of Nikon bodies, <em>I
        felt as I was a betrayer</em>. Beyond the blue mood there's also the objective fact of dealing with a very
      different camera body and a different lens system. I mean, <em>every single Nikon lens I own has got a reputation</em>;
      it might be excellent or just good, but it's well known in the community (I've never bought a lens that just hit
      the shelves). Sony E-mount has been criticized for having mostly mediocre lenses until this year, when some good
      and possibly excellent products have been released. But it's too early for them to have a reputation. This
      scenario makes evaluation harder because you have many independent variables: the feeling of the camera, its
      performance, the sensor, the lens... So now I'm concentrating on a single aspect at a time. This first day has
      been dedicated to the overall feeling of the camera.</p>
    <h4>The viewfinder</h4>
    <p>The <strong>electronic viewfinder</strong> (EVF) is to me the most important feature of the camera: I don't like
      at all shooting through the live view on the back display (but in a few exceptions), not counting that it's almost
      impossible to have a good readout of a LCD display in full sun. I recently evaluated some magnification loupes
      with eye-cup that could be coupled to a rear LCD, but I concluded that they are not a good solution: I fear the
      low quality of cheaper products, while good quality stuff is expensive, above all heavy; furthermore the pixel
      count of rear displays (1MP in the best cases) is not enough for a crisp view experience. </p>
    <p>The EVF of the NEX-6 has got very good reviews and it's considered one of the best around for this class of
      cameras. The first thing that I noted is its big magnification (1.09x): the Nikon D7000 is 0.94x, but the big
      difference is to the Nikon D5100 and its 0.58x. As a consequence you feel more “immersed” in the picture, in my
      opinion a very valuable feature for a better composition. Colour rendering is very similar to the thing that you'd
      see through an optic viewfinder (unless you change settings, see below). While its 2,3+ megapixels aren't enough
      to prevent some artefacts such as staggered lines, the feeling is very good and the motion lag is moderate. If the
      subject doesn't contain straight lines, as it happens with natural landscapes, the EVF is not disturbing at all. </p>
    <p>The EVF has got several advantages:</p>
    <ul>
      <li><strong>It can display extra information</strong>, in addition to those available in optical viewfinders:
        practically everything you'd like to see in the rear display (including a live histogram). The amount of
        information is configurable and you can cycle among different configurations. To tell the truth, the histogram
        is quite rough, but it really helps you in <strong><a href="http://www.luminous-landscape.com/tutorials/expose-right.shtml">“exposing
            to the right”</a></strong>, which is my favourite way of operating (even though I often fail and under
        expose for an excess of prudence): not having to look at the rear display after each shot you can operate
        without detaching your eye from the viewfinder. </li>
      <li><strong>It is probably closer to what the final image will look like</strong>: for instance, if you're
        severely over or under exposing, you'll immediately notice. If you want, you can apply processing effects to the
        image in the EVF — for instance, if you want to shoot in black &amp; white you can see the world in black &amp;
        white — not bad, indeed. Note that this affects the creation of JPEG images, but if you shoot RAW as I do all
        the effects can be overridden in post processing (this is pretty easy in Lightroom with automatically presets
        applied during import).</li>
      <li><strong>It can be used as a poor man IR viewer to look in the dark</strong> — just crank the ISO up. More than
        being fun, I suppose it can be useful for manual focusing in the darkness (for instance, I remember some pain a
        few years ago when I tried to shoot at fireflies).</li>
      <li>On the other hand, <strong>it's no longer dangerous to shoot at the setting sun</strong> -
        when it's still bright it can damage your eyes through the mirror and the pentaprism in a DSLR. I also suppose
        the EVF will help in understanding how the setting sun will be rendered in your picture (this kind of shot is
        always a tricky exposure).</li>
    </ul>
    <p>By default the EVF turns automatically on when your eye approaches the viewfinder. It comes with a decent
      eye-cup, such as the one provided with the Nikon D7000 and D5100. With those bodies I replaced the standard
      eye-cup with a larger rubber shell that does a better job of blocking the external light — I suppose there should
      be equivalent products in the Sony third-party market. </p>
    <p>The price to pay for the EVF is higher energy consumption, so with a full battery charge the NEX-6 shoots a
      dramatically smaller number of photos. This is not secondary, as the NEX-6 is not shipped with a battery charger:
      the body itself does the job by means of a USB cable, but of course you can't use the camera while it's charging.
      And it takes 4/5 hours to complete the job. The problem can be mitigated buying a second battery and a separate
      charger — the aftermarket offers non-Sony cheap products (20/30€).</p>
    <h4>Handling</h4>
    <p>While my primary aim with mirrorless is to enjoy smaller and lighter gear, I was worried about how a smaller
      camera would feel in my hands: too small might mean hard to handle. The NEX-6 is very well designed from this
      respect: there's a faux leather grip and I can seize the camera with the classic approach, right hand on the grip
      and left hand under the lens. Very good. The camera doesn't even feel too light in my hands: indeed, it feels
      heavier than I expected (this is because it's smaller and the brain elaborates the weight of objects together with
      their size). But the NEX-6 with the 35mm lens is really lighter, roughly 300g lighter than the D5100 with the same
      focal Nikkor DX lens: thus there's a real relief for my back when carrying the gear around.</p>
    <div class="article-photo"><img class="framedPhoto" src="$mediaLink(relativePath='/20131026-0018.jpg')$" />
      <p class="title">The NEX-6 compared with the Nikon D5100, front view.<br />
        The are not many differences, mostly it's just the NEX-6 not having the protuberance for the viewfinder.</p>
    </div>
    <div class="article-photo"><img class="framedPhoto" src="$mediaLink(relativePath='/20131026-0019.jpg')$" />
      <p class="title">The NEX-6 compared to the Nikon D5100, top view.<br />
        Here the difference is significant, thanks to the removal of the mirror and the pentaprism.</p>
    </div>
    <p>The NEX-6 is well built, with no parts that feels cheap, including the coverages of compartments such as the
      battery bay or the USB/HDMI connectors (here Nikon lacks something). Controls are stiff at the right degree;
      buttons and dials give the proper feedback. Is it rugged? I don't know. Some parts are in magnesium, like the
      Nikon D7000. I know it makes a difference because I dropped both the Nikon D7000 and a D5000, whose body is in
      pure plastic, on concrete; the former survived well with just some almost invisible wounds; the latter broke
      beyond repair. The NEX-6 feels as it sits in the middle.</p>
    <p>There weren't many surprises about the way the camera operates, as I was able to read tons of detailed
      information on the web before the buy. You can work with two dials: one at the top, under the mode selector, and
      another, vertically arranged, in the back (that is also used for selecting menu items). For instance:</p>
    <ul>
      <li> in manual mode the top dial controls aperture, the back dial shutter speed; </li>
      <li>in shutter-priority mode the top dial controls the shutter speed and the back dial is not active;</li>
      <li>in aperture-priority mode the top dial controls the aperture and the back dial is not active. <br />
      </li>
    </ul>
    <p>Very good: this is similar to the dual dial approach of the D7000 and actually better than the D5100 that sports
      a single dial. </p>
    <p>The first stupid thing is auto ISO: <strong>it cannot be enabled in manual mode</strong> (this and other stupid
      things are purportedly designed probably for marketing reasons, since implementing them is just a matter of
      software and would cause no additional costs in the manufacturing of the product). Fortunately, the ISO setting
      can be changed by pressing the right edge of the back dial, dialling the new value and confirming with the “Ok”
      button at the centre of the dial. You have to get acquainted to that, but in the end it can be done quickly
      without removing the eye from the viewfinder. I'd call it “two dials and a half” operations. </p>
    <p>As you independently set aperture, shutter speed and ISO, a linear gauge in the viewfinder indicates how far you
      are from the supposed optimal exposure, in thirds of EV; but as I previously said, to guess the better exposure
      it's probably more effective to use the live histogram.</p>
    <p>In A-priority and S-priority modes EV override can be set by pressing the lower part of the back dial and then
      rotating it: I can get used to this with no problems. <span style="text-decoration: line-through;">A further
        stupid thing is that, in auto ISO mode, <strong>you cannot see the actual ISO value</strong>: this makes it
        impossible to verify whether you're using settings that produce too much noise. You have to shot, review (a 2sec
        review of the latest photo can be automatically enabled, also in the viewfinder) and look at what ISO has been
        actually used. Added to the fact that you <strong>can't set an upper limit for auto ISO</strong>, this mostly
        defeats the auto ISO purpose, in my opinion.</span> ISO Auto in these modes can be activated, but it's not what
      you expect: it's the camera that choose both ISO and the shutter speed, giving you no control — this at least is
      what I'm understanding. You can see the actual value of the ISO when you half-press the shutter button. It's also
      impossible to customize an upper limit for the ISO. In this way, the feature seems pretty useless.</p>
    <p>Don't ask me about P-mode as I've never used it in this century.</p>
    <h4>Auto-focusing</h4>
    <p>The first, real bad thing is about focusing: the NEX-6 <strong>cannot disengage auto-focusing from the trigger</strong>
      (as far as I understand, the NEX-7 can). In the past years I've always disengaged the auto-focusing from the
      trigger, as I find it's the better way to operate in “focus and recompose” mode. Especially when using a tripod,
      you wish to focus once and then shot a sequence, even minutes later, without re-engaging the autofocus. With the
      D7000, which allows two user setting presets, I even have a “still” preset where auto-focusing is disengaged and a
      “motion” preset (mainly used for birds and other animals) where it is engaged. </p>
    <p>Nothing like this can be done with the NEX-6: furthermore <strong>the camera auto-focuses autonomously</strong>,
      even when you don't press any button. <strong>You have to revert to the “half-press trigger” technique</strong>,
      after having disengaged the exposure lock from the trigger (it's an option reachable in the menus): now
      half-pressing the trigger locks the auto-focusing, doesn't lock the exposure and makes it possible to recompose.
      This is indeed the default technique for most cameras, but it always failed for me. A quick try with a moving
      subject failed, as you can see in the photo below: I tried to focus on the boat in the lower right corner, but it
      didn't work: instead, the focus is on the crane in foreground. </p>
    <div class="nwXsltMacro_Photo">
      <p class="nwXsltMacro_Photo_photoId">20131025-0014</p>
      <p class="nwXsltMacro_Photo_title">$IPTC.IIM.title$</p>
      <p class="nwXsltMacro_Photo_shootingData">$shootingData$</p>
    </div>
    <p>A better option for being in full control is to <strong>switch to manual focusing mode after the desired focus
        has been achieved</strong>. With the Nikon D7000 it's just a matter of touching a switch; additionally AF-S
      lenses sport a further switch on the barrel. Instead, the NEX-6 has got no specific switch for manual focus; the
      operation can be assigned to the Fn button, but it does not give direct access to the feature: instead, it recalls
      a customized menu, so changing a focusing mode in this way requires more than a single gesture. The “Auto-Exposure
      Lock” button actually can be reprogrammed to give direct access to focus mode switching with no intermediate
      gestures, but this means to give up with exposure locking because this feature, funny enough, can't be assigned to
      one of the Fn menus. It's quite annoying that Sony designers messed up this.</p>
    <div class="digression">
      <p>This cumbersome focusing behaviour has been discussed over and over in photography forums, being a perfect
        example of <a href="http://fabriziogiudici.it/blog/too-much-technology-may-push-culture-backward/">the pitfalls
          of a society over-saturated with technology</a>... Having full control of a camera means to be able to create
        a photo in the exact way you want: this is supposed to be photography, after all. Focus control needn't to be
        implemented in the very same way as they did for decades, nevertheless I want to still be in easy, full control.
        But most people seem unable to understand the concept, preferring a technological gadget that “does everything
        for you”: so in response to complains many answered things such as “you don't need focus control”, “just trust
        the camera”, “recomposing and manual focusing is a thing from the '70s” and so on. Sigh. <a href="http://www.newrepublic.com/article/113299/leon-wieseltier-commencement-speech-brandeis-university-2013">As
          Leon Wieseltier said</a>: <em>The technological mentality that has become the [...] [people's] worldview
          instructs us to prefer practical questions to questions of meaning — to ask of things not if they are [...]
          good or evil, but how they work.</em></p>
    </div>
    <p><strong>The focusing point selection is similar but more complicated</strong> than with the Nikon D7000 and
      D5100. With those cameras the back dial could be directly used to move the focus point (and the central button can
      be programmed to quickly reset it to the central sensor). With the NEX-6 the function must be activated by
      pressing the bottom function key and requires a final confirmation before you're again able to take a picture: two
      additional button clicks that make it impossible to move the sensor while shooting (a thing that works perfectly
      fine with my Nikon bodies). There's a mode for tracking moving subjects, but it requires to position the cursor
      over them — with the given premises, it's quite impossible to use it for fast subjects such as flying birds. But
      it was clear to me that the NEX-6 is basically for still subjects — that's why I plan to use it mainly for
      landscapes.</p>
    <p>A good thing is that NEX-6 <strong>features both contrast- and phase-detection</strong> auto focusing (unlike
      the NEX-7), the latter being faster. But it doesn't work with all the lenses — for instance, the option is
      disabled when I mount my Sigma 30mm F2.8 DN | A. It might be just a matter of updating the firmware, though.</p>
    <div class="nwXsltMacro_Photo">
      <p class="nwXsltMacro_Photo_photoId">20131025-0017</p>
      <p class="nwXsltMacro_Photo_title">$IPTC.IIM.title$</p>
      <p class="nwXsltMacro_Photo_shootingData">$shootingData$</p>
    </div>
    <h4>Manual focusing</h4>
    <p>Where the NEX-6 possibly shines is with manual focusing. First, you have to know that manual focusing is operated
      with the usual ring on the lens, but — unlike Nikon DSLRs — <strong>it doesn't mechanically engage the lens</strong>.
      Instead, it provides the electronics in the camera with the required information and then the camera operates on
      the lens: it's called <strong><a href="http://photo.stackexchange.com/questions/26428/how-does-a-fly-by-wire-focusing-lens-work">“focus
          by wire”</a></strong> and it's a typical feature of mirrorless cameras. I was really worried about it: typical
      good idea with possibly crappy implementation. This is not the case: I must admit it works flawlessly. It
      partially depends on the lens, of course, and the Sigma lens I've bought has got a very, very smooth focus ring;
      the lens focuses after the finger rotation with no apparent lag. What's very good is that focus changes slowly as
      the ring rotates, allowing for precise operations, recalling the feel of the old manual lenses (or the first
      Nikkor AF lenses that were also good with manual focus), a feeling that was unfortunately lost with the latest
      Nikkor AF-S products: their ring goes from minimum to maximum distance in a few degrees, so precision focusing has
      become hard — as if manual focusing was implemented only to write one more feature on the specifications, rather
      than be an effective thing. </p>
    <p>But good news isn't over. With an optional feature, manual focusing automatically activates <strong>magnification
        in the viewfinder</strong>, as if you had a <a href="http://www.apug.org/forums/forum44/100736-im-little-confused-about-focusing-loupes-help.html">focusing
        loupe</a>: 4.8x and 9.6x magnifications can be cycled with a single button press. This is possibly very
      valuable, I suppose specially for macro, even though I have to try it with the tripod: without it, the image is
      very unstable (even with a normal focal as my current lens). Perhaps stabilised lenses (the Sigma I've bought
      isn't) give a better experience.</p>
    <p>A further manual focusing feature is <strong>“focus peaking”</strong>: the viewfinder is supposed to display
      coloured markings over the parts of the subject that are in sharp focus. This would be a fantastic aid — but it
      doesn't work all the times. In fact, the camera doesn't really show focused portions, rather areas with a high
      local contrast: often they are really those in sharp focus, but I've seen that there can be frequently false
      positives or negatives. In any case, the photo below has been manually focused with this technique over the
      “MASTIFF” boat name, and indeed that portion of the picture is in focus. Note that I've never been able to
      manually focus with high precision, probably because of a lack of sensibility in my eyes, so I really appreciate
      this feature.</p>
    <div class="nwXsltMacro_Photo">
      <p class="nwXsltMacro_Photo_photoId">20131025-0011</p>
      <p class="nwXsltMacro_Photo_title">$IPTC.IIM.title$</p>
      <p class="nwXsltMacro_Photo_shootingData">$shootingData$</p>
    </div>
    <p>Also the next photo was manually focused with the peaking aid: I aimed at the radar at the top of the yacht, and
      it is properly focused.</p>
    <div class="nwXsltMacro_Photo">
      <p class="nwXsltMacro_Photo_photoId">20131025-0005</p>
      <p class="nwXsltMacro_Photo_title">$IPTC.IIM.title$</p>
      <p class="nwXsltMacro_Photo_shootingData">$shootingData$</p>
    </div>
    <p>A third focusing mode, called DMF, seems to offer the best of two worlds, manual and automatic: half-press the
      trigger, wait for autofocus lock and then you can manually override with the lens ring (with an annoyance: you
      must keep the trigger half-pressed). Basically a similar feature to the AF-S Nikkor lenses, whose focus ring, when
      touched, disengages autofocus. If you want you also get focus peaking assist and image magnification,
      automatically activated. </p>
    <p>The manual focusing features could be really useful. To be further tested on the field.</p>
    <h4>Other small problems </h4>
    <p>The cameras takes about three seconds to be operative when turned on or when waking up from power-save mode: this
      is quite annoying, even though it should not be a problem with landscapes. </p>
    <p>The lack of a mirror which “protects” the sensor makes the latter more prone to get dirty because of dust. Modern
      cameras offer a feature that “shakes” the sensor to get rid at least of the larger dust particles and my Nikon
      D7000 and D5100 offer the convenient option of automatically engaging this cleaning procedure when turned on. This
      is not possible with the NEX-6: cleaning must be always manually engaged and, when terminated, it forces you to
      turn the camera off; as far I understand it's because at this point you are supposed to blow some air over the
      sensor to get rid of the particles that have been detached. </p>
    <p>It seems there's no option to save the current settings to the memory card for later retrieval: too bad, as I
      fear that a software update (there's one that I should apply) could reset everything.</p>
    <p>The camera is WiFi-enabled and a feature makes it possible to push photos to my MacBook Pro without USB
      connection. Too bad I wasn't able to make it work yet.</p>
    <p>The menu system deserves the bad reviews it got.</p>
    <p>There are many further features of the camera, such as HDR, panorama, and smile-triggered focusing that I've not
      talked about — and I will never use.</p>
    <h4>Sensor and lens</h4>
    <p>As I wrote above, this first session was not intended to evaluate the sensor nor the lens; I've got just some
      preliminary belly sensation. There's something that I don't like with the colours and the sharpness is only good.
      But I still have to work with Lightroom — perhaps I'll have to prepare a specific profile for the new camera — and
      having shot hand held, with a shutter speed at the motion blur limit for the focal, a sharpness analysis doesn't
      make sense (that's why I'm not providing the full resolution images). In any case, the Sigma 30mm F2.8 DN won't
      survive long in my bag: not because it's bad, but because if at the end of my evaluation I'll approve the Sony
      mirrorless system I'll replace it with the Sony E 35mm f/1.8; it's a bit optically superior, but above all it
      sports stabilisation.</p>
    <h4>Conclusion</h4>
    <p>No big surprises, the camera at first sight is what I expected. I have to get acquainted to a few things that
      must be done in a different way than I was used, but it's really annoying that a couple of frequently used
      operations, such as switching focus mode, require too many gestures. Some cheap bodies are designed in this way
      because they have just a handful of physical buttons/dials; but this is not the case of the NEX-6, whose
      limitations are only due to a far-from-perfect software user interface.</p>
    <p> The next thing to understand is the quality of the sensor and the lens(es): on this purpose I have to go with a
      side-by-side comparison with my Nikon gear.</p>
  </body>
</html>
